#!/usr/bin/ruby
# frozen_string_literal: true

#
# Gitlab Plugin
# ==
# Author: Marco Peterseil
# Created: 03-2017
# License: GPLv3 - http://www.gnu.org/licenses
# URL: https://gitlab.com/6uellerBpanda/check_gitlab
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

require 'optparse'
require 'net/https'
require 'json'

version = 'v0.1.1'

# optparser
banner = <<~HEREDOC
  check_gitlab #{version} [https://gitlab.com/6uellerBpanda/check_gitlab]\n
  This plugin checks various parameters of Gitlab\n
  Mode:
    health       Check the Gitlab web endpoint for health
    services     Check if any service of 'gitlab-ctl status' is down
    ci-pipeline  Check duration of a CI pipeline
    ci-runner    Check status of CI runners

  Usage: #{File.basename(__FILE__)} [options]
HEREDOC

options = {}
OptionParser.new do |opts| # rubocop:disable  Metrics/BlockLength
  opts.banner = banner.to_s
  opts.separator ''
  opts.separator 'Options:'
  opts.on('-s', '--address ADDRESS', 'Gitlab address') do |s|
    options[:address] = s
  end
  opts.on('-t', '--token TOKEN', 'Access token') do |t|
    options[:token] = t
  end
  opts.on('-i', '--id ID', 'Project ID') do |i|
    options[:id] = i
  end
  opts.on('-k', '--insecure', 'No ssl verification') do |k|
    options[:insecure] = k
  end
  opts.on('-m', '--mode MODE', 'Mode to check') do |m|
    options[:mode] = m
  end
  opts.on('-w', '--warning WARNING', 'Warning threshold') do |w|
    options[:warning] = w
  end
  opts.on('-c', '--critical CRITICAL', 'Critical threshold') do |c|
    options[:critical] = c
  end
  opts.on('-v', '--version', 'Print version information') do
    puts "check_gitlab #{version}"
  end
  opts.on('-h', '--help', 'Show this help message') do
    puts opts
  end
  ARGV.push('-h') if ARGV.empty?
end.parse!

# check args for api calls
unless options[:mode] == 'services' || ARGV.empty?
  raise OptionParser::MissingArgument if options[:address].nil? || options[:token].nil?
end

# check gitlab
class CheckGitlab
  def initialize(options)
    @options = options
    health_check
    ci_pipeline_check
    ci_runner_check
    services_check
  end

  # Define some helper methods for Nagios with appropriate exit codes
  def ok_msg(message)
    puts "OK - #{message}"
    exit 0
  end

  def crit_msg(message)
    puts "Critical - #{message}"
    exit 2
  end

  def warn_msg(message)
    puts "Warning - #{message}"
    exit 1
  end

  def unk_msg(message)
    puts "Unknown - #{message}"
    exit 3
  end

  def check_stats(value, message)
    if value > @options[:critical].to_i
      crit_msg(message).to_s
    elsif value > @options[:warning].to_i
      warn_msg(message).to_s
    else
      ok_msg(message).to_s
    end
  end

  # create url
  def url(path:)
    uri = URI("#{@options[:address]}/#{path}")
    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true
    http.verify_mode = OpenSSL::SSL::VERIFY_NONE if @options[:insecure]
    request = Net::HTTP::Get.new(uri.request_uri)
    request.add_field 'PRIVATE-TOKEN', @options[:token] if @options[:mode] != 'health'
    @response = http.request(request)
  rescue StandardError => msg
    unk_msg(msg)
  end

  # check some known response from gitlab
  def check_http_response
    case @response
    when Net::HTTPUnauthorized
      unk_msg(@response.message).to_s
    when Net::HTTPNotFound
      unk_msg(@response.message).to_s
    when Net::HTTPInternalServerError
      warn_msg(@response.message).to_s
    end
  end

  # init http req
  def http_connect(path:)
    url(path: path)
    check_http_response
  end

  # health check
  #
  def health_check
    return unless @options[:mode] == 'health'
    http_connect(path: "-/readiness?token=#{@options[:token]}")
    unhealthy_probes = JSON.parse(@response.body).reject { |_k, v| v['status'] == 'ok' }
    if unhealthy_probes.empty?
      ok_msg('Gitlab probes are in healthy state')
    else
      warn_msg(unhealthy_probes.map { |k, _v| k }.join(', ') + ' probe has problems')
    end
  end

  # ci-pipeline duration check
  #
  def ci_pipeline_check # rubocop:disable  Metrics/AbcSize
    return unless @options[:mode] == 'ci-pipeline'
    http_connect(path: "api/v4/projects/#{@options[:id]}/pipelines")
    # get latest pipeline
    http_connect(path: "api/v4/projects/#{@options[:id]}/pipelines/#{JSON.parse(@response.body).first['id']}")
    ci_pipeline_data = JSON.parse(@response.body)
    perfdata = "| duration=#{ci_pipeline_data['duration']}s;#{@options[:warning]};#{@options[:critical]}"
    check_stats(ci_pipeline_data['duration'], "Pipeline ##{ci_pipeline_data['id']} took #{ci_pipeline_data['duration']}s #{perfdata}")
  end

  # ci-runner check
  #
  def ci_runner_check
    return unless @options[:mode] == 'ci-runner'
    http_connect(path: 'api/v4/runners/all')
    runners = JSON.parse(@response.body)
    unactive_runners, active_runners = runners.partition { |item| item['active'] == false }
    check_stats(unactive_runners.count, "#{active_runners.count} runner active" + ", #{unactive_runners.count} runner not active")
  end

  # services check
  #
  def gitlab_ctl_status
    `sudo gitlab-ctl status`.scan(/(?:down: )(\w+)/)
  rescue StandardError => msg
    unk_msg(msg)
  end

  def services_check
    return unless @options[:mode] == 'services'
    if gitlab_ctl_status.any?
      down_srvc = gitlab_ctl_status.join(', ')
      crit_msg("#{down_srvc} is down")
    else
      ok_msg('All services are running')
    end
  end
end

CheckGitlab.new(options)
